#include <tchar.h>
#include "Book.h"

Book::Book() { name_of_book = autor_of_book = year_of_book = state = kind_of_book = NULL; }// ��� ����������� = 0
Book::Book(wchar_t *nob, wchar_t *aob, wchar_t *yob, wchar_t *kob, wchar_t * s)
{
	/*������� ���������� �������� � ���� ������*/
	name_of_book = new wchar_t[150];
	wcscpy(name_of_book, !(nob) ? L"not indicated" : nob);

	autor_of_book = new wchar_t[150];
	wcscpy(autor_of_book, !(aob) ? L"not indicated" : aob);

	year_of_book = new wchar_t[150];
	wcscpy(year_of_book, !(yob) ? L"not indicated" : yob);

	kind_of_book = new wchar_t[150];
	wcscpy(kind_of_book, !(kob) ? L"not indicated" : kob);

	state = new wchar_t[2];
	if (wcscmp(s, L"1") == 0)
		wcscpy(state, s);
	if (wcscmp(s, L"2") == 0)
		wcscpy(state, s);
	if (wcscmp(s, L"3") == 0)
		wcscpy(state, s);
	if (wcscmp(s, L"4") == 0)
		wcscpy(state, s);

}
Book::Book(wchar_t *nob, wchar_t *aob, wchar_t *yob, wchar_t *kob)
{
	/*������� ���������� �������� � ���� ������*/
	name_of_book = new wchar_t[150];
	wcscpy(name_of_book, !(nob) ? L"not indicated" : nob);

	autor_of_book = new wchar_t[150];
	wcscpy(autor_of_book, !(aob) ? L"not indicated" : aob);

	year_of_book = new wchar_t[150];
	wcscpy(year_of_book, !(yob) ? L"not indicated" : yob);

	kind_of_book = new wchar_t[150];
	wcscpy(kind_of_book, !(kob) ? L"not indicated" : kob);

	state = new wchar_t[2];
	wcscpy(state, L"1");
}
//Book::~Book() {
//	delete[]name_of_book;
//
//	delete[]autor_of_book;
//
//	delete[]year_of_book;
//
//	delete[]kind_of_book;
//
//	delete[]state;
//}


wchar_t * Book::GetNameOfBook(){return (name_of_book)? name_of_book : L"not indicated";}// ��������� �������� ����� ��� *�� ������*
wchar_t * Book::GetAutorOfBook(){return (autor_of_book) ? autor_of_book : L"not indicated";}// ��������� ������ ����� ��� *�� ������*
wchar_t * Book::GetYearOfBook(){return (year_of_book) ? year_of_book : L"not indicated";}// ��������� ��� �������� ����� ��� *�� ������*
wchar_t * Book::GetKindOfBook(){return (kind_of_book) ? kind_of_book : L"not indicated";}// ��������� ��� ����� ��� *�� ������*
wchar_t * Book::GetState() { return (state)? state:L"1"; }// ��������� ������ ����� ��� 1

void Book::SetBook(wchar_t *nob, wchar_t *aob, wchar_t *yob, wchar_t *kob, wchar_t* s)
{
	/*������� ���������� �������� � ���� ������*/
	if (name_of_book == NULL)
		name_of_book = new wchar_t[150];
	wcscpy(name_of_book, !(nob) ? L"not indicated" : nob);

	if (autor_of_book == NULL)
		autor_of_book = new wchar_t[150];
	wcscpy(autor_of_book, !(aob) ? L"not indicated" : aob);

	if (year_of_book == NULL)
		year_of_book = new wchar_t[150];
	wcscpy(year_of_book, !(yob) ? L"not indicated" : yob);

	if (kind_of_book == NULL)
		kind_of_book = new wchar_t[150];
	wcscpy(kind_of_book, !(kob) ? L"not indicated" : kob);

	if (state == NULL)
		state = new wchar_t[2];
	if (wcscmp(s, L"1") == 0)
		wcscpy(state, L"1");
	if (wcscmp(s, L"2") == 0)
		wcscpy(state, s);
	if (wcscmp(s, L"3") == 0)
		wcscpy(state, s);
	if (wcscmp(s, L"4") == 0)
		wcscpy(state, s);
}
void Book::SetBook(wchar_t *nob, wchar_t *aob, wchar_t *yob, wchar_t *kob)
{
	/*������� ���������� �������� � ���� ������*/
	if(name_of_book == NULL)
		name_of_book = new wchar_t[150];
	wcscpy(name_of_book, !(nob) ? L"not indicated" : nob);

	if (autor_of_book == NULL)
		autor_of_book = new wchar_t[150];
	wcscpy(autor_of_book, !(aob) ? L"not indicated" : aob);
	
	if (year_of_book == NULL)
		year_of_book = new wchar_t[150];
	wcscpy(year_of_book, !(yob) ? L"not indicated" : yob);

	if (kind_of_book == NULL)
		kind_of_book = new wchar_t[150];
	wcscpy(kind_of_book, !(kob) ? L"not indicated" : kob);

	if (state == NULL)
		state = new wchar_t[2];
	wcscpy(state, L"1");
}
//	����� �������� �����
void Book::SetNameOfBook(wchar_t *nob)
{
	if (name_of_book == NULL)
		name_of_book = new wchar_t[150];
	wcscpy(name_of_book, !(nob) ? L"not indicated" : nob);
}
//	����� ������ �����
void Book::SetAutorOfBook(wchar_t *aob)
{
	if (autor_of_book == NULL)
		autor_of_book = new wchar_t[150];
	wcscpy(autor_of_book, !(aob) ? L"not indicated" : aob);
}
//	����� ��� �����
void Book::SetYearOfBook(wchar_t *yob)
{
	if (year_of_book == NULL)
		year_of_book = new wchar_t[150];
	wcscpy(year_of_book, !(yob) ? L"not indicated" : yob);
}
//	����� ��� �����
void Book::SetKindOfBook(wchar_t *kob)
{
	if (kind_of_book == NULL)
		kind_of_book = new wchar_t[150];
	wcscpy(kind_of_book, !(kob) ? L"not indicated" : kob);
}
//	����� ������ �����
void Book::SetState(wchar_t* s)
{
	if (state == NULL)
		state = new wchar_t[2];
	if (wcscmp(s, L"1") == 0)
		wcscpy(state, L"1");
	if (wcscmp(s, L"2") == 0)
		wcscpy(state, s);
	if (wcscmp(s, L"3") == 0)
		wcscpy(state, s);
	if (wcscmp(s, L"4") == 0)
		wcscpy(state, s);
}


