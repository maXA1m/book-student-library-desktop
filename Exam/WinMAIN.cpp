#include <windows.h> /*��������� ���������� WinAPI*/
#include <tchar.h> /*��������� ���������� ���������*/
#include <vector>
#include "User.h"/*��������� ���������� �������������*/
#include "Book.h"/*��������� ���������� ����*/
#include "resource.h"/*��������� ���������� ��������*/

HINSTANCE hInst;

HWND hwndStudent; //���������� ���� ��������
int index = 0; // ����������, ������� ������ ������ ��������������� ������������
bool bookser = 1;// ����-���������� ��� ������������ ���������� ����
const wchar_t login_admin[50] = L"admin"; //	����� ������
const wchar_t password_admin[50] = L"admin";//	������ ������

std::vector<Book> all_books; // �� ������� ������ ���� �������� ����

std::vector<User> users(5); // �� ������� ������ ���� ������������

void books_init();//	������� ��� ������� ������������� ����
void logins_passwords_init();//	������� ��� ������� ������������� �������������

BOOL CALLBACK DlgProc(HWND, UINT, WPARAM, LPARAM);//	�������� ���������� ��� ����� ����
BOOL CALLBACK Dlg_Student_Proc(HWND, UINT, WPARAM, LPARAM);//	�������� ���������� ��� ������������ ����
BOOL CALLBACK Dlg_Admin_Proc(HWND, UINT, WPARAM, LPARAM);//	�������� ���������� ��� �����-����
BOOL CALLBACK Dlg_Confirm_Proc(HWND, UINT, WPARAM, LPARAM);//	�������� ���������� ��� ���� ������������ ���� ���������
BOOL CALLBACK Dlg_Add_New_Book_Proc(HWND, UINT, WPARAM, LPARAM);//	�������� ���������� ��� ���� ���������� ���� �������������


int WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInst, LPTSTR lpszCmdLine, int nCmdShow)
{
	return DialogBox(hInstance, MAKEINTRESOURCE(IDD_DIALOG1), NULL, DlgProc);// ����������� ����������� ����
}

// ���������� ���� �����
BOOL CALLBACK DlgProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HWND hEdit1 = GetDlgItem(hWnd, IDC_EDIT1);// ���������� ���� �����
	HWND hEdit2 = GetDlgItem(hWnd, IDC_EDIT2);// ���������� ���� �����
	HWND hEdit3 = GetDlgItem(hWnd, IDC_ERROR_EDIT);// ���������� ���� �����
	switch (message)//	���� ��� ��������� ���������
	{
	case WM_CLOSE:
		EndDialog(hWnd, 0);
		return TRUE;


	case WM_INITDIALOG: {
		logins_passwords_init();//	������������� �������������
		hInst = GetModuleHandle(NULL);
		SetWindowText(hEdit3, L"hello");// ���� ����� ����� ���� HELLO
		if (bookser) // ����������� ���������� ����
			books_init();
	}
		return TRUE;

	case WM_COMMAND:

		switch (LOWORD(wParam))/*		���� ��� ������			*/
		{
			case IDC_LOGIN_BUTTON_GO:{

				// ��������� ����� ������, ��������� � ��������� ����
				int length_login = SendMessage(hEdit1, WM_GETTEXTLENGTH, 0, 0);
				int length_password = SendMessage(hEdit2, WM_GETTEXTLENGTH, 0, 0);

				// ������� ������ ������������ �������
				TCHAR *pBuffer_login = new TCHAR[length_login + 1];
				TCHAR *pBuffer_password = new TCHAR[length_password + 1];

				// � ���������� ������ ��������� �����, �������� � �������� ����
				GetWindowText(hEdit1, pBuffer_login, length_login + 1);
				GetWindowText(hEdit2, pBuffer_password, length_password + 1);
				if (lstrlen(pBuffer_login) && lstrlen(pBuffer_password)) {
					
					for (int i = 0; i < users.size(); i++) {
						if (wcscmp(pBuffer_login, users[i].GetNameOfUser()) == 0 && wcscmp(pBuffer_password, users[i].GetPasswordOfUser()) == 0) {

							index = i;// ����� ������� ������������
							SetWindowText(hEdit3, L"user");
							EndDialog(hWnd, 0);
							DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_DIALOG_STUDENT), hWnd, Dlg_Student_Proc, 0);

						}
						else
							SetWindowText(hEdit3, L"error");
					}// ���� ������������

					if (wcscmp(pBuffer_login, login_admin) == 0 && wcscmp(pBuffer_password, password_admin) == 0) {
						SetWindowText(hEdit3, L"admin");
						EndDialog(hWnd, 0);
						DialogBoxParam(hInst, MAKEINTRESOURCE(DIALOG_ADMIN), hWnd, Dlg_Admin_Proc, 0);
					}
					else
						SetWindowText(hEdit3, L"error");
					// ���� ������

				}
				else {
					SetWindowText(hEdit3, L"error");// ���� ������� �� ���������� ������, ���������� ������
				}
				delete[] pBuffer_login;// ������ ��������� ����������
				delete[] pBuffer_password;// ������ ��������� ����������
			}
				break;
		}
		return TRUE;


	}
	return FALSE;
}
 // ���������� ���� ��������
BOOL CALLBACK Dlg_Student_Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	hwndStudent = hWnd;//���������� ���� ������������

	// ������� ����������� ��������� ����������
	HWND new_book = GetDlgItem(hWnd, NEW_BOOK);
	HWND book_book = GetDlgItem(hWnd, BOOK_BOOK);
	HWND my_book = GetDlgItem(hWnd, MY_BOOK);
	switch (message)//	���� ��� ��������� ���������
	{
	case WM_CLOSE:
		EndDialog(hWnd, 0);
		return TRUE;

	case WM_INITDIALOG: {

		for (int i = 4; i < 13; i++)
			all_books[i].SetState(L"4");

		SetWindowText(new_book, L"My Books");
		for (int i = 0; i < all_books.size(); i++) {

			SendMessage(book_book, LB_ADDSTRING, 0, LPARAM(all_books[i].GetNameOfBook()));

			if (wcscmp(all_books[i].GetState(), L"4") == 0) 
				SendMessage(my_book, LB_ADDSTRING, 0, LPARAM(all_books[i].GetNameOfBook()));

		}

		return TRUE;
	}
	case WM_COMMAND:

		switch (LOWORD(wParam))/*		���� ��� ������			*/
		{
		case STUDENT_CANCEL_BUTT: 
			EndDialog(hWnd, 0);
			break;


		case CONFIRM_BUTTON: 
			DialogBoxParam(hInst, MAKEINTRESOURCE(CONFIRM_DLG), hWnd, Dlg_Confirm_Proc, 0);// �������� ���� ������������� ����
			break;


		case BOOK_BUTTON: {

			/* ��������� ���������� ��������� ��������� � ������ � ������������� ������� */
			int nCount = SendMessage(book_book, LB_GETSELCOUNT, 0, 0);

			/* ������� ������ ������������ ������� ��� �������� �������� ��������� ��������� ������ */
			int *p = new int[nCount];

			/* �������� ������������ ������ ��������� ���������� ��������� ������ */
			SendMessage(book_book, LB_GETSELITEMS, nCount, LPARAM(p));

			for (int i = 0; i < nCount; i++)
			{
				// ��������� ������ ������ �������� ������
				int length = SendMessage(book_book, LB_GETTEXTLEN, p[i], 0);

				// ������� ������ ������������ �������
				TCHAR *pBuffer = new TCHAR[length + 1];

				/* � ���������� ������ ��������� ����� ���������� �������� ������ */
				SendMessage(book_book, LB_GETTEXT, p[i], LPARAM(pBuffer));

				for (int j = 0; j < all_books.size(); j++) {
					if (wcscmp(pBuffer, all_books[j].GetNameOfBook()) == 0 && wcscmp(all_books[j].GetState(),L"4") != 0) {
						all_books[j].SetState(L"2");
						MessageBox(hWnd, pBuffer,
							TEXT("You book this book"), MB_OK | MB_ICONINFORMATION);
					}
					else if(wcscmp(pBuffer, all_books[j].GetNameOfBook()) == 0 && wcscmp(all_books[j].GetState(), L"4") == 0){
						MessageBox(hWnd, pBuffer,
							TEXT("You have this book"), MB_OK | MB_ICONINFORMATION);
					}
				}
				delete[] pBuffer;
			}// �������� ����� �� �������� � ������� ��, ���� ����� � ���� �� ����

		}
			break;


		case ABOUT_BOOK:
		{
			int index = SendMessage(book_book, LB_GETCURSEL, 0, 0);
			int index2 = SendMessage(my_book, LB_GETCURSEL, 0, 0);

			if (index != LB_ERR) // ������ �� ������� ������?
			{

				/* ��������� ���������� ��������� ��������� � ������ � ������������� ������� */
				int nCount = SendMessage(book_book, LB_GETSELCOUNT, 0, 0);

				/* ������� ������ ������������ ������� ��� �������� �������� ��������� ��������� ������ */
				int *p = new int[nCount];

				/* �������� ������������ ������ ��������� ���������� ��������� ������ */
				SendMessage(book_book, LB_GETSELITEMS, nCount, LPARAM(p));
				for (int i = 0; i < nCount; i++)
				{
					// ��������� ������ ������ �������� ������
					int length = SendMessage(book_book, LB_GETTEXTLEN, p[i], 0);

					// ������� ������ ������������ �������
					TCHAR *pBuffer = new TCHAR[length + 1];

					/* � ���������� ������ ��������� ����� ���������� �������� ������ */
					SendMessage(book_book, LB_GETTEXT, p[i], LPARAM(pBuffer));
					for (int j = 0; j < all_books.size(); j++) {
						if (wcscmp(pBuffer, all_books[j].GetNameOfBook()) == 0) {

							TCHAR *pBufferTMP2 = new TCHAR[300];
							*pBufferTMP2 = 0;

							wcscat(pBufferTMP2, L"Name: ");
							wcscat(pBufferTMP2, all_books[j].GetNameOfBook());
							wcscat(pBufferTMP2, L"\nAutor: ");
							wcscat(pBufferTMP2, all_books[j].GetAutorOfBook());
							wcscat(pBufferTMP2, L"\nCreate year: ");
							wcscat(pBufferTMP2, all_books[j].GetYearOfBook());
							wcscat(pBufferTMP2, L"\nKind of book: ");
							wcscat(pBufferTMP2, all_books[j].GetKindOfBook());

							MessageBox(hWnd, pBufferTMP2,
								TEXT("About book"), MB_OK | MB_ICONINFORMATION);

							delete[] pBufferTMP2;
						}
					}
					delete[] pBuffer;
				}
			}

			if (index2 != LB_ERR) // ������ �� ������� ������?
			{

				/* ��������� ���������� ��������� ��������� � ������ � ������������� ������� */
				int nCount2 = SendMessage(my_book, LB_GETSELCOUNT, 0, 0);

				/* ������� ������ ������������ ������� ��� �������� �������� ��������� ��������� ������ */
				int *p2 = new int[nCount2];

				/* �������� ������������ ������ ��������� ���������� ��������� ������ */
				SendMessage(my_book, LB_GETSELITEMS, nCount2, LPARAM(p2));
				for (int i = 0; i < nCount2; i++)
				{
					// ��������� ������ ������ �������� ������
					int length2 = SendMessage(my_book, LB_GETTEXTLEN, p2[i], 0);

					// ������� ������ ������������ �������
					TCHAR *pBuffer2 = new TCHAR[length2 + 1];

					/* � ���������� ������ ��������� ����� ���������� �������� ������ */
					SendMessage(my_book, LB_GETTEXT, p2[i], LPARAM(pBuffer2));
					for (int j = 0; j < all_books.size(); j++) {
						if (wcscmp(pBuffer2, all_books[j].GetNameOfBook()) == 0) {

							TCHAR *pBufferTMP = new TCHAR[300];
							*pBufferTMP = 0;

							wcscat(pBufferTMP, L"Name: ");
							wcscat(pBufferTMP, all_books[j].GetNameOfBook());
							wcscat(pBufferTMP, L"\nAutor: ");
							wcscat(pBufferTMP, all_books[j].GetAutorOfBook());
							wcscat(pBufferTMP, L"\nCreate year: ");
							wcscat(pBufferTMP, all_books[j].GetYearOfBook());
							wcscat(pBufferTMP, L"\nKind of book: ");
							wcscat(pBufferTMP, all_books[j].GetKindOfBook());

							MessageBox(hWnd, pBufferTMP,
								TEXT("About book"), MB_OK | MB_ICONINFORMATION);

							delete[] pBufferTMP;
						}
					}
					delete[] pBuffer2;
				}
			}

		}
			break;


		case RELOGIN_STUDENT: 
			EndDialog(hWnd, 0);
			DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, DlgProc, 0);//�������� ���� �����
			break;
		}
		return TRUE;


	}
	return FALSE;
}
 // ���������� ���� ������
BOOL CALLBACK Dlg_Admin_Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HWND new_books = GetDlgItem(hWnd, NEW_REQESTS);// ���������� ������ �������
	switch (message)//	���� ��� ��������� ���������
	{
	case WM_CLOSE:
		EndDialog(hWnd, 0);
		return TRUE;


	case WM_INITDIALOG: 
		for (int i = 0; i < all_books.size(); i++) {// �������� � ������ �����, ������� ������� ������������
			if (wcscmp(all_books[i].GetState(), L"2") == 0)
				SendMessage(new_books, LB_ADDSTRING, 0, LPARAM(all_books[i].GetNameOfBook()));
		}
		return TRUE;


	case WM_COMMAND:
		switch (LOWORD(wParam))/*		���� ��� ������			*/
		{

		case CANCEL_ADMIN:
			EndDialog(hWnd, 0);
			break;


		case ADD_NEW_BOOK: 
			DialogBoxParam(hInst, MAKEINTRESOURCE(ADD_NEW_BOOK_DIALOG), hWnd, Dlg_Add_New_Book_Proc, 0);
			break;


		case ABOUT_BOOK_ADMIN: {

			/* ��������� ���������� ��������� ��������� � ������ � ������������� ������� */
			int nCount = SendMessage(new_books, LB_GETSELCOUNT, 0, 0);

			/* ������� ������ ������������ ������� ��� �������� �������� ��������� ��������� ������ */
			int *p = new int[nCount];

			/* �������� ������������ ������ ��������� ���������� ��������� ������ */
			SendMessage(new_books, LB_GETSELITEMS, nCount, LPARAM(p));
			for (int i = 0; i < nCount; i++)
			{
				// ��������� ������ ������ �������� ������
				int length = SendMessage(new_books, LB_GETTEXTLEN, p[i], 0);

				// ������� ������ ������������ �������
				TCHAR *pBuffer = new TCHAR[length + 1];

				/* � ���������� ������ ��������� ����� ���������� �������� ������ */
				SendMessage(new_books, LB_GETTEXT, p[i], LPARAM(pBuffer));
				for (int j = 0; j < all_books.size(); j++) {
					if (wcscmp(pBuffer, all_books[j].GetNameOfBook()) == 0) {

						TCHAR *pBufferTMP2 = new TCHAR[300];
						*pBufferTMP2 = 0;

						wcscat(pBufferTMP2, L"Name: ");
						wcscat(pBufferTMP2, all_books[j].GetNameOfBook());
						wcscat(pBufferTMP2, L"\nAutor: ");
						wcscat(pBufferTMP2, all_books[j].GetAutorOfBook());
						wcscat(pBufferTMP2, L"\nCreate year: ");
						wcscat(pBufferTMP2, all_books[j].GetYearOfBook());
						wcscat(pBufferTMP2, L"\nKind of book: ");
						wcscat(pBufferTMP2, all_books[j].GetKindOfBook());

						MessageBox(hWnd, pBufferTMP2,
							TEXT("About book"), MB_OK | MB_ICONINFORMATION);

						delete[] pBufferTMP2;
					}// ����� ���������� � �����
				}
				delete[] pBuffer;
			}
		}
			break;


		case REQEST_BOOK: {

			/* ��������� ���������� ��������� ��������� � ������ � ������������� ������� */
			int nCount = SendMessage(new_books, LB_GETSELCOUNT, 0, 0);

			/* ������� ������ ������������ ������� ��� �������� �������� ��������� ��������� ������ */
			int *p = new int[nCount];

			/* �������� ������������ ������ ��������� ���������� ��������� ������ */
			SendMessage(new_books, LB_GETSELITEMS, nCount, LPARAM(p));
			for (int i = 0; i < nCount; i++)
			{
				// ��������� ������ ������ �������� ������
				int length = SendMessage(new_books, LB_GETTEXTLEN, p[i], 0);

				// ������� ������ ������������ �������
				TCHAR *pBuffer = new TCHAR[length + 1];

				/* � ���������� ������ ��������� ����� ���������� �������� ������ */
				SendMessage(new_books, LB_GETTEXT, p[i], LPARAM(pBuffer));
				for (int j = 0; j < all_books.size(); j++) {
					if (wcscmp(pBuffer, all_books[j].GetNameOfBook()) == 0) {
						all_books[j].SetState(L"3");
						TCHAR *pBufferTMP2 = new TCHAR[300];
						*pBufferTMP2 = 0;

						wcscat(pBufferTMP2, L"You request this book");
						wcscat(pBufferTMP2, L"\nName: ");
						wcscat(pBufferTMP2, all_books[j].GetNameOfBook());
						wcscat(pBufferTMP2, L"\nAutor: ");
						wcscat(pBufferTMP2, all_books[j].GetAutorOfBook());
						wcscat(pBufferTMP2, L"\nCreate year: ");
						wcscat(pBufferTMP2, all_books[j].GetYearOfBook());
						wcscat(pBufferTMP2, L"\nKind of book: ");
						wcscat(pBufferTMP2, all_books[j].GetKindOfBook());

						MessageBox(hWnd, pBufferTMP2,
							TEXT("Request book"), MB_OK | MB_ICONINFORMATION);

						delete[] pBufferTMP2;
					}
				}// ����� ����� ������������
				delete[] pBuffer;
			}
		}
			break;


		case RELOGIN_ADMIN: 
			EndDialog(hWnd, 0);
			DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_DIALOG1), hWnd, DlgProc, 0);// ������� � ���� �����
			break;
		}
		return TRUE;


	}
	return FALSE;
}
 // ���������� ���� �������������
BOOL CALLBACK Dlg_Confirm_Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HWND confirm_books = GetDlgItem(hWnd, CONFIRM_LIST);// ���������� ������ ���� ��� �������������
	switch (message)//	���� ��� ��������� ���������
	{
	case WM_CLOSE:
		/*����� �������� ���� ������������� � �������� ���� ��������*/
		EndDialog(hWnd, 0); 
		return TRUE;


	case WM_INITDIALOG: 
		for (int i = 0; i < all_books.size(); i++) {
			if (wcscmp(all_books[i].GetState(), L"3") == 0) 
				SendMessage(confirm_books, LB_ADDSTRING, 0, LPARAM(all_books[i].GetNameOfBook()));
		}// �������� � ������ ����� ��� �������������
		return TRUE;


	case WM_COMMAND:
		switch (LOWORD(wParam))/*		���� ��� ������			*/
		{
			case CONFIRM_BUTT_DLG: {

				/* ��������� ���������� ��������� ��������� � ������ � ������������� ������� */
				int nCount = SendMessage(confirm_books, LB_GETSELCOUNT, 0, 0);

				/* ������� ������ confirm_books ������� ��� �������� �������� ��������� ��������� ������ */
				int *p = new int[nCount];

				/* �������� ������������ ������ ��������� ���������� ��������� ������ */
				SendMessage(confirm_books, LB_GETSELITEMS, nCount, LPARAM(p));

				for (int i = 0; i < nCount; i++)
				{
					// ��������� ������ ������ �������� ������
					int length = SendMessage(confirm_books, LB_GETTEXTLEN, p[i], 0);

					// ������� ������ ������������ �������
					TCHAR *pBuffer = new TCHAR[length + 1];

					/* � ���������� ������ ��������� ����� ���������� �������� ������ */
					SendMessage(confirm_books, LB_GETTEXT, p[i], LPARAM(pBuffer));

					for (int j = 0; j < all_books.size(); j++) {
						if (wcscmp(pBuffer, all_books[j].GetNameOfBook()) == 0 && wcscmp(all_books[j].GetState(), L"3") == 0) {
							all_books[j].SetState(L"4");
							MessageBox(hWnd, pBuffer,
								TEXT("You confirm this book"), MB_OK | MB_ICONINFORMATION);
							EndDialog(hWnd, 0);
							EndDialog(hwndStudent, 0);

							DialogBoxParam(hInst, MAKEINTRESOURCE(IDD_DIALOG_STUDENT), hWnd, Dlg_Student_Proc, 0);
						}
					}	/*
						�������� ����� � ������ ���� ��������(������������)
						������ �� ����� ��������� �� �������� ������������� �����
						*/
					delete[] pBuffer;
				}

			}
				break;


			case CANSEL_CONFIRM_DLG: 
				/*����� �������� ���� ������������� � �������� ���� ��������*/
				EndDialog(hWnd, 0);
				break;
		}
		return TRUE;


	}
	return FALSE;
}
//	���������� ���� ���������� �����
BOOL CALLBACK Dlg_Add_New_Book_Proc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	HWND hEditName = GetDlgItem(hWnd, NAME_OF_BOOK);// ���������� ���� �����
	HWND hEditAutor = GetDlgItem(hWnd, AUTOR_OF_BOOK);// ���������� ���� �����
	HWND hEditKind = GetDlgItem(hWnd, KIND_OF_BOOK);// ���������� ���� �����
	HWND hEditYear = GetDlgItem(hWnd, YEAR_OF_CREATE);// ���������� ���� �����
	switch (message)//	���� ��� ��������� ���������
	{
	case WM_CLOSE:
		EndDialog(hWnd, 0);
		return TRUE;


	case WM_INITDIALOG: 

		return TRUE;


	case WM_COMMAND:

		switch (LOWORD(wParam))/*		���� ��� ������			*/
		{

		case OK_NEW_BOOK: {
			// ��������� ����� ������, ��������� � ��������� ����
			int length_name = SendMessage(hEditName, WM_GETTEXTLENGTH, 0, 0);
			int length_autor = SendMessage(hEditAutor, WM_GETTEXTLENGTH, 0, 0);
			int length_kind = SendMessage(hEditKind, WM_GETTEXTLENGTH, 0, 0);
			int length_year = SendMessage(hEditYear, WM_GETTEXTLENGTH, 0, 0);

			// ������� ������ ������������ �������
			TCHAR *pBuffer_name = new TCHAR[length_name + 1];
			TCHAR *pBuffer_autor = new TCHAR[length_autor + 1];
			TCHAR *pBuffer_kind = new TCHAR[length_kind + 1];
			TCHAR *pBuffer_year = new TCHAR[length_year + 1];

			// � ���������� ������ ��������� �����, �������� � �������� ����
			GetWindowText(hEditName, pBuffer_name, length_name + 1);
			GetWindowText(hEditAutor, pBuffer_autor, length_autor + 1);
			GetWindowText(hEditKind, pBuffer_kind, length_kind + 1);
			GetWindowText(hEditYear, pBuffer_year, length_year + 1);

			if (length_name && length_autor && length_kind && length_year) {
				if (length_name > 149 || length_autor > 149 || length_kind > 149 || length_year > 149) {
					MessageBox(hWnd, L"One or more text forms too long",
						TEXT("!ERROR!"), MB_OK | MB_ICONINFORMATION);
					if (length_name > 149)
						SetWindowText(hEditName, L" ");
					if (length_autor > 149)
						SetWindowText(hEditAutor, L" ");
					if (length_kind > 149)
						SetWindowText(hEditKind, L" ");
					if (length_year > 149)
						SetWindowText(hEditYear, L" ");
				}
				else {

					for (int i = 0; i < all_books.size(); i++) {
						if (wcscmp(pBuffer_name, all_books[i].GetNameOfBook()) == 0 && wcscmp(pBuffer_autor, all_books[i].GetAutorOfBook()) == 0) {
							MessageBox(hWnd, L"You have this book!",
								TEXT("!ERROR!"), MB_OK | MB_ICONINFORMATION);
							break;
						}// ������ ���� ��� ���������� ����� �����
						else {
							all_books.push_back(Book(pBuffer_name, pBuffer_autor, pBuffer_kind, pBuffer_year));
							MessageBox(hWnd, L"New book added!",
								TEXT("Added"), MB_OK | MB_ICONINFORMATION);
							EndDialog(hWnd, 0);
							break;
						}// �������� �����
					}
				}
			}
			else {// ������ ���� �� ��������� ��� ����
				MessageBox(hWnd, L"Fill in all fields!",
					TEXT("!ERROR!"), MB_OK | MB_ICONINFORMATION);
			}


			delete[] pBuffer_name;// ������ ��������� ����������
			delete[] pBuffer_autor;// ������ ��������� ����������
			delete[] pBuffer_kind;// ������ ��������� ����������
			delete[] pBuffer_year;// ������ ��������� ����������
		}
			break;


		case CANCEL_NEW_BOOK:
			EndDialog(hWnd, 0);
			break;

		}
		return TRUE;
	}
	return FALSE;
}



void books_init() {
	all_books.reserve(70);
	/*������������� 5 ����*/
	all_books.push_back(Book(L"If Not for You", L"Bob Dylan", L"1989", L"Documentary"));
	all_books.push_back(Book(L"The Sellout: A Novel", L"Paul Beatty", L"1989", L"Novel"));
	all_books.push_back(Book(L"Ancillary Mercy (Imperial Radch)", L"Ann Leckie", L"1345", L"fantastic"));
	all_books.push_back(Book(L"Black Flags: The Rise of ISIS", L"Joby Warrick", L"1989", L"Drame"));
	all_books.push_back(Book(L"Eileen: A Novel", L"Ottessa Moshfegh", L"1989", L"Novel"));
	all_books.push_back(Book(L"Hot Milk", L"Deborah Levy", L"2003", L"Poem"));
	all_books.push_back(Book(L"The Lyrics: 1961-2012", L"Stephen Hawking", L"1961-2012", L"Lyrics"));
	all_books.push_back(Book(L"The Underground Railroad : A Novel", L"Colson Whitehead", L"1989", L"Novel"));
	all_books.push_back(Book(L"The Throwback Special: A Novel", L"Chris Bachelder", L"1989", L"Novel"));
	all_books.push_back(Book(L"The Bible � Old Testament", L"Nicolae Sfetcu", L"1989", L"Science"));
	all_books.push_back(Book(L"Stalker", L"Conan Doil", L"2003", L"Fantastic"));
	all_books.push_back(Book(L"My Brief History", L"Stephen Hawking", L"1989", L"Science"));
	all_books.push_back(Book(L"The Grand Design", L"Stephen Hawking", L"1989", L"Science"));
	all_books.push_back(Book(L"A Brief History of Time", L"Stephen Hawking", L"1989", L"Science"));
	all_books.push_back(Book(L"A Briefer History of Time", L"Stephen Hawking", L"1989", L"Science"));
	all_books.push_back(Book(L"Black Holes: The Reith lectures", L"Stephen Hawking", L"1989", L"Science"));
	all_books.push_back(Book(L"George and the Blue Moon", L"Stephen Hawking", L"1989", L"fantastic"));
	all_books.push_back(Book(L"George and the Unbreakable Code", L"Stephen Hawking", L"1989", L"fantastic"));
	all_books.push_back(Book(L"The Universe in a Nutshell", L"Stephen Hawking", L"1989", L"Science"));
	all_books.push_back(Book(L"Black Holes and Baby Universes", L"Stephen Hawking", L"1989", L"Science"));
	bookser = 0;
}
void logins_passwords_init() {
	/*������������� 5 �������*/
	users[0].SetAllOfUser(L"USER", L"user", L"max.mironenko3@gmail.com", L"2000 year");
	users[1].SetAllOfUser(L"Dima Novikov", L"12345678", L"dima.novikov@gmail.com", L"1989 year");
	users[2].SetAllOfUser(L"Daniel Batkilov", L"12345678", L"daniel.batkilov@gmail.com", L"2000 year");
	users[3].SetAllOfUser(L"Luda Ivanova", L"12345678", L"luda.ivanova@gmail.com", L"2000 year");
	users[4].SetAllOfUser(L"Max Mironenko", L"12345678", L"max.mironenko3@gmail.com", L"2000 year");
}