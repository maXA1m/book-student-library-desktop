#pragma once
class Book
{
	// �������� �����
	wchar_t* name_of_book;
	// ����� �����
	wchar_t* autor_of_book;
	// ��� �������� �����
	wchar_t* year_of_book;
	// ���� �����
	wchar_t* kind_of_book;
	// ��������� �����(���������)
	wchar_t* state;// 1 - default	2 - book	3 - new		4 - student
public:
	// ����������� �����������
	Book();
	// ������������ ����������� � ����������
	Book(wchar_t*, wchar_t*, wchar_t*, wchar_t*, wchar_t*);
	// ������������ ����������� ��� ���������
	Book(wchar_t*, wchar_t*, wchar_t*, wchar_t*);
	// ����������
	//~Book();


	/*		���/��� ������ ��� ���������� ������		*/
	wchar_t* GetNameOfBook();
	wchar_t* GetAutorOfBook();
	wchar_t* GetYearOfBook();
	wchar_t* GetKindOfBook();
	wchar_t* GetState();

	void SetBook(wchar_t*, wchar_t*, wchar_t*, wchar_t*, wchar_t*);
	void SetBook(wchar_t*, wchar_t*, wchar_t*, wchar_t*);
	void SetNameOfBook(wchar_t*);
	void SetAutorOfBook(wchar_t*);
	void SetYearOfBook(wchar_t*);
	void SetKindOfBook(wchar_t*);
	void SetState(wchar_t*);
};

