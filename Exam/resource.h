//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Exam.rc
//
#define IDD_DIALOG1                     101
#define IDD_DIALOG_STUDENT              103
#define DIALOG_ADMIN                    105
#define CONFIRM_DLG                     107
#define ADD_NEW_BOOK_DIALOG             109
#define IDC_EDIT1                       1001
#define IDC_EDIT2                       1002
#define IDC_LOGIN_BUTTON_GO             1003
#define IDC_ERROR_EDIT                  1004
#define STUDENT_CANCEL_BUTT             1005
#define STUDENT_BOOK_BUTT               1007
#define BOOK_BOOK                       1008
#define MY_BOOK                         1012
#define BOOK_BUTTON                     1013
#define NEW_BOOK                        1014
#define ABOUT_BOOK                      1015
#define RELOGIN_STUDENT                 1016
#define RELOGIN_ADMIN                   1017
#define ADD_NEW_BOOK                    1018
#define NEW_REQESTS                     1021
#define REQEST_BOOK                     1023
#define ABOUT_BOOK_ADMIN                1024
#define CANCEL_ADMIN                    1025
#define BOOKS_TO_REQUEST                1026
#define CONFIRM_BUTTON                  1027
#define CONFIRM_LIST                    1028
#define CONFIRM_BUTT_DLG                1029
#define CANSEL_CONFIRM_DLG              1030
#define KIND_OF_BOOK                    1031
#define YEAR_OF_CREATE                  1032
#define NAME_OF_BOOK                    1033
#define AUTOR_OF_BOOK                   1034
#define OK_NEW_BOOK                     1035
#define CANCEL_NEW_BOOK                 1036

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        111
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1037
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
