#pragma once
#include <vector>
#include "Book.h"
class User
{
	//	��� ������������
	wchar_t* name_of_user;
	//	������ ������������
	wchar_t* password_of_user;
	//	����� ������������
	wchar_t* email_of_user;
	//	���� �������� ������������
	wchar_t* birthdate_of_user;

public:
	std::vector<Book> books_of_user;// �� ������� ������ ��� �������� ����
	// ����������� �����������
	User();
	// ������������� ����������� 
	User(wchar_t*, wchar_t*, wchar_t*, wchar_t*);
	//	����������
	//~User();

	/*		���/��� ������ ��� ���������� ������		*/
	wchar_t* GetNameOfUser();
	wchar_t* GetPasswordOfUser();
	wchar_t* GetEmailOfUser();
	wchar_t* GetBirthdateOfUser();
	Book GetBookOfUserByIndex(int);

	void SetNameOfUser(wchar_t*);
	void SetPasswordOfUser(wchar_t*);
	void SetEmailOfUser(wchar_t*);
	void SetBirthdateOfUser(wchar_t*);
	void SetBookOfUser(wchar_t *nob, wchar_t *aob, wchar_t *yob, wchar_t *kob, wchar_t * s);
	void SetAllOfUser(wchar_t*, wchar_t*, wchar_t*, wchar_t*);
};

